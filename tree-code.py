from numpy import loadtxt
from sklearn.tree import DecisionTreeClassifier 
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
dataset = loadtxt('pima-indians-diabetes.data.csv', delimiter=",")
X = dataset[:,0:8]
Y = dataset[:,8]
test_size = 0.2
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=test_size)
model = DecisionTreeClassifier(criterion = "gini", random_state = 100,max_depth=3, min_samples_leaf=5) 
print(model)
model.fit(X_train, y_train) 
y_pred = model.predict(X_test)
predictions = [round(value) for value in y_pred]
accuracy = accuracy_score(y_test, predictions)
print("%.2f" % (accuracy * 100.0))

